# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2023-05-26)

### Features

- add project and gitlab pipeline ([f5a3069](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/f5a306940b12b0b6f398793ea9bb14f0e2f1cb60))
- added pipelines ([dd63a43](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/dd63a43a960cc6cf79f5545585f88c79b7e24018))
- fix parallel pipeline process ([0f75478](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/0f754782c6fa61f25373193b1961f111252b9ac0))
- fix pipline ([da904de](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/da904dee9d29501cf305f1579ce6ed92a196c748))
- fo=x node ([d06edb3](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/d06edb35995f6a74d67e2c433371873c299527d2))

### Bug Fixes

- build stage aws error ([afd2a0c](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/afd2a0c9d6a14110c7f63bc312e8b0d73c21c1ff))
- pipeline deploy ([67dbcb3](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/67dbcb3e38678deceda475627f459a039731387a))
- pipline ([e50afc7](https://gitbud.epam.com/oleksandra_gryshchuk/shop-angular-cloudfront/commit/e50afc77591459971bf138cb493698777f74f6ce))
